import React, { useContext, createContext } from 'react';
import { ThemeProvider } from 'styled-components';

import lightTheme from '../styles/themes/lightTheme';
import darkTheme from '../styles/themes/darkTheme';

export const ThemeContext = createContext(null);

export const ThemeContextProvider = ({ children }) => {
  const [theme, setTheme] = React.useState(lightTheme);

  //
  const [loadingAuth, setLoading] = React.useState(true);

  // ComponentDidMount
  React.useEffect(() => {
    setLoading(false);
  }, []);

  //
  const setDarkMode = async () => {
    setTheme(darkTheme);
  };

  //
  const setLightMode = async () => {
    setTheme(lightTheme);
  };

  //
  const values = React.useMemo(
    () => ({
      theme,
      setLightMode,
      setDarkMode,
    }),
    [theme],
  );

  // Finally, return the interface that we want to expose to our other components
  return (
    <ThemeProvider theme={theme}>
      <ThemeContext.Provider value={values}>{children}</ThemeContext.Provider>
    </ThemeProvider>
  );
};

//
export function useThemeContext() {
  const context = useContext(ThemeContext);
  return context;
}
