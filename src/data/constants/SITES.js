export const SITES = {
  ADMIN: 'Adminsitrative',
  ORGANIZATION: 'Organization',
  AGENT: 'Agent',
  CUSTOMER: 'Customer',

  ROUTER_ADMIN: '/admin',
  ROUTER_ORGANIZATION: '/admin',
  ROUTER_AGENT: '/admin',
  ROUTER_CUSTOMER: '/site',

  ROUTER_LOGIN: '/login',
  ROUTER_DASHBOARD: '/dashboard',
};
