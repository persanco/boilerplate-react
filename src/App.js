import React from 'react';
import { PageLanding } from './pages/common/Landing';
import Routes from './pages/Routes';

export const App = () => (
  <React.Suspense fallback={null}>
    <Routes />
  </React.Suspense>
);

export default App;
