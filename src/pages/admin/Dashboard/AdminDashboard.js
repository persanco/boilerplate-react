import React from 'react';
import PropTypes from 'prop-types';
import { Box } from './AdminDashboard.styles';
import { Text } from '../../../components/typography/Text';

const _AdminDashboard = ({ id = 'AdminDashBoardPage__' }) => (
  <Box>
    <Box id={id}>
      <Text>Admin Dashboard</Text>
    </Box>
  </Box>
);

_AdminDashboard.propTypes = {
  id: PropTypes.string,
};

const AdminDashboard = React.memo(_AdminDashboard);

export { AdminDashboard };
