import React from 'react';
import { LayoutLanding } from '../../../layouts/LayoutLanding';
import { SectionLandingSlide } from '../../../sections/common/LandingSlide';

const LandingPage = () => (
  <LayoutLanding id="LandingPage__">
    <SectionLandingSlide id="LandingPage__" />
  </LayoutLanding>
);

const PageLanding = React.memo(LandingPage);

export { PageLanding };
