import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { RowLeft, RowRight, Row } from '../../../components/boxes';
import { SITES } from '../../../data/constants';
import { LayoutLogin } from '../../../layouts/LayoutLogin';

import { Button, ButtonText } from '../../../components/buttons';
import { Text } from '../../../components/typography';
import { InputText } from '../../../components/inputs/InputText';

import { Container } from './PageLogin.styles.js';

const _PageLogin = ({ type = SITES.CUSTOMER }) => {
  const { history } = useHistory();
  const [isLoading, setIsLoading] = React.useState(true);

  //
  React.useLayoutEffect(() => {
    setIsLoading(false);

    return () => null;
  }, []);

  const handleLogin = () => {
    if (history) return;

    switch (type) {
      case SITES.ADMIN:
        history.push(`${SITES.ROUTER_ADMIN}${SITES.ROUTER_DASHBOARD}`);
        break;

      case SITES.ORGANIZATION:
        history.push(`${SITES.ROUTER_ORGANIZATION}${SITES.ROUTER_DASHBOARD}`);
        break;

      case SITES.AGENT:
        history.push(`${SITES.ROUTER_AGENT}${SITES.ROUTER_DASHBOARD}`);
        break;

      default:
        history.push(`${SITES.ROUTER_CUSTOMER}${SITES.ROUTER_DASHBOARD}`);
        break;
    }
  };

  const handleForgot = () => {
    //
  };

  return (
    <LayoutLogin>
      {!!isLoading && (
        <Row>
          <Text>Loading...</Text>
        </Row>
      )}
      {!isLoading && (
        <Container id="PageLogin__Form">
          <RowLeft id="PageLogin__Form--DivBack">
            <Button id="PageLogin__Form__DivBack--Button">
              <Text>Back</Text>
            </Button>
          </RowLeft>
          <Row>
            <InputText />
          </Row>
          <Row>
            <InputText />
          </Row>
          <RowRight>
            <ButtonText onClick={() => handleForgot()}>
              <Text>Forgot your password?</Text>
            </ButtonText>
          </RowRight>
          <Row>
            <Button onClick={() => handleLogin()}>
              <Text>LOGIN</Text>
            </Button>
          </Row>
        </Container>
      )}
    </LayoutLogin>
  );
};

_PageLogin.propTypes = {
  type: PropTypes.string.isRequired,
};

const PageLogin = React.memo(_PageLogin);

export { PageLogin };
