import styled from 'styled-components';
import { Box } from '../../../components/boxes';

export const Container = styled(Box)`
  border-radius: 5px;
  background-color: #f0f0f0;
  flex-direction: column;
`;
