import React from 'react';
import PropTypes from 'prop-types';
import { Box } from './AgentDashboard.styles';
import { Text } from '../../../components/typography/Text';

const _AgentDashboard = ({ id = 'AgentDashBoardPage__' }) => (
  <Box>
    <Box id={id}>
      <Text>Agent Dashboard</Text>
    </Box>
  </Box>
);

_AgentDashboard.propTypes = {
  id: PropTypes.string,
};

const AgentDashboard = React.memo(_AgentDashboard);

export { AgentDashboard };
