import styled from 'styled-components';
import { Box as BaseComponent } from '../../../components/boxes/Box';

export const Box = styled(BaseComponent)`
  width: 100%;
`;
