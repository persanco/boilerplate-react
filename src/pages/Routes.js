import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { SITES } from '../data/constants/SITES';

// Routes
import { Login } from './common/Login';
import { AdminDashboard } from './admin/Dashboard';
import { OrganizationDashboard } from './organization/Dashboard';
import { AgentDashboard } from './agent/Dashboard';
import { CustomerDashboard } from './customer/Dashboard';
import { NotFound } from './common/NotFound/NotFound';
import AuthenticatedRoute from './AuthenticatedRoute';
import UnauthenticatedRoute from './UnauthenticatedRoute';
//
import { PageLogin } from './common/Login';
import { PageLanding } from './common/Landing';

export default () => (
  <Switch>
    <Route exact path="/">
      <PageLanding />
    </Route>
    {/* LOGIN */}
    <UnauthenticatedRoute path={`${SITES.ROUTER_ADMIN}${SITES.ROUTER_LOGIN}`}>
      <PageLogin type={SITES.ADMIN} />
    </UnauthenticatedRoute>
    <UnauthenticatedRoute path={`${SITES.ROUTER_ORGANIZATION}${SITES.ROUTER_LOGIN}`}>
      <PageLogin type={SITES.ORGANIZATION} />
    </UnauthenticatedRoute>
    <UnauthenticatedRoute path={`${SITES.ROUTER_AGENT}${SITES.ROUTER_LOGIN}`}>
      <PageLogin type={SITES.AGENT} />
    </UnauthenticatedRoute>
    <UnauthenticatedRoute path={`${SITES.ROUTER_CUSTOMER}${SITES.ROUTER_LOGIN}`}>
      <PageLogin type={SITES.CUSTOMER} />
    </UnauthenticatedRoute>
    {/* DASHBOARD */}
    <AuthenticatedRoute path={`${SITES.ROUTER_ADMIN}${SITES.ROUTER_DASHBOARD}`}>
      <AdminDashboard />
    </AuthenticatedRoute>
    <AuthenticatedRoute path={`${SITES.ROUTER_ORGANIZATION}${SITES.ROUTER_DASHBOARD}`}>
      <OrganizationDashboard />
    </AuthenticatedRoute>
    <AuthenticatedRoute path={`${SITES.ROUTER_AGENT}${SITES.ROUTER_DASHBOARD}`}>
      <AgentDashboard />
    </AuthenticatedRoute>
    <AuthenticatedRoute path={`${SITES.ROUTER_CUSTOMER}${SITES.ROUTER_DASHBOARD}`}>
      <CustomerDashboard />
    </AuthenticatedRoute>
    {/* Finally, catch all unmatched routes */}
    <Route>
      <NotFound />
    </Route>
  </Switch>
);
