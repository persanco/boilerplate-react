import React from 'react';
import PropTypes from 'prop-types';
import { Box } from './CustomerDashboard.styles';
import { Text } from '../../../components/typography/Text';

const _CustomerDashboard = ({ id = 'CustomerDashBoardPage__' }) => (
  <Box>
    <Box id={id}>
      <Text>Customer Dashboard</Text>
    </Box>
  </Box>
);

_CustomerDashboard.propTypes = {
  id: PropTypes.string,
};

const CustomerDashboard = React.memo(_CustomerDashboard);

export { CustomerDashboard };
