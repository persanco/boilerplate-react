import React from 'react';
import PropTypes from 'prop-types';
import { Box } from './OrganizationDashboard.styles';
import { Text } from '../../../components/typography/Text';

const _OrganizationDashboard = ({ id = 'OrganizationDashBoardPage__' }) => (
  <Box>
    <Box id={id}>
      <Text>Organization Dashboard</Text>
    </Box>
  </Box>
);

_OrganizationDashboard.propTypes = {
  id: PropTypes.string,
};

const OrganizationDashboard = React.memo(_OrganizationDashboard);

export { OrganizationDashboard };
