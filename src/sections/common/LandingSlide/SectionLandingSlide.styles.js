import styled from 'styled-components';
import { Text } from '../../../components/typography/Text';

export const Container = styled.div`
  width: 100vw;
  height: 90vh;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const SideContainer = styled.div`
  width: 50%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const TextContainer = styled.div`
  max-width: calc(100% - 40px);
  width: 80%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0px 20px;
`;

export const Description = styled(Text)`
  color: #2f2f2f;
  font-size: 16px;
  line-height: 16px;
  padding: 10px 0px;
`;
