import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '../../../components/buttons';
import { Image } from '../../../components/images/Image';
import { Title, Text } from '../../../components/typography';
import Company from '../../../assets/images/company.jpg';

import {
  Container, TextContainer, SideContainer, Description,
} from './SectionLandingSlide.styles';

const SectionLandingSlide = ({ onClick }) => (
  <Container id="SectionLandingSlide">
    <SideContainer id="SectionLandingSlide__SideContainer__">
      <Image src={Company} id="SectionLandingSlide__SideContainer__Image" />
    </SideContainer>
    <SideContainer id="SectionLandingSlide__SideContainer">
      <TextContainer>
        <Title>ERES UN CLIENTE?</Title>
        <Description>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores facere quia laboriosam delectus dolor
          nisi, obcaecati tempore hic suscipit esse, praesentium corrupti totam. Ut sequi ea aspernatur obcaecati
          accusantium quam!
        </Description>
        <Button onClick={onClick}>
          <Text>Eres un cliente?</Text>
          <br />
          <Text>Ingresa Totalmente Gratis.</Text>
        </Button>
      </TextContainer>
      <TextContainer>
        <Title>ERES UNA EMPRESA?</Title>
        <Description>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Asperiores facere quia laboriosam delectus dolor
          nisi, obcaecati tempore hic suscipit esse, praesentium corrupti totam. Ut sequi ea aspernatur obcaecati
          accusantium quam!
        </Description>
        <Button onClick={onClick}>
          <Text>30 dias gratis.</Text>
          <br />
          <Text>Crea tu empresa.</Text>
        </Button>
      </TextContainer>
    </SideContainer>
  </Container>
);

SectionLandingSlide.propTypes = {
  onButton: PropTypes.func,
};

export { SectionLandingSlide };
