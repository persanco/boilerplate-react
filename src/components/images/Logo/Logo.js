import React from 'react';
import PropTypes from 'prop-types';
import { Container, Image } from './Logo.styles';
import logo from '../../../assets/images/logo.png';

const LogoPNG = ({ size = '512' }) => (
  <Container>
    <Image size={size} src={logo} />
  </Container>
);

LogoPNG.propTypes = {
  size: PropTypes.string,
};

const Logo = React.memo(LogoPNG);

export { Logo };
