import styled from 'styled-components';

export const Container = styled.div`
  width: 10vw;
  height: 10vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Image = styled.img`
  width: 80%;
  height: 80%;
  max-width: 10vh;
`;
