import styled from 'styled-components';
import { Text } from '../Text';

export const Title = styled(Text)`
  font-size: 24px;
`;
