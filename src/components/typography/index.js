import { Text } from './Text';
import { Title } from './Title';
import { H1 } from './H1';

export { Text, Title, H1 };
