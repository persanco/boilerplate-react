import styled from 'styled-components';
import { Text } from '../Text';

export const H1 = styled(Text)`
  font-size: 32px;
`;
