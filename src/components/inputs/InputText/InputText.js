import React from 'react';
import PropTypes from 'prop-types';
import { StyledComponent } from './InputText.styles';

const InputText = (props) => <StyledComponent {...props} />;

InputText.propTypes = {};

export { InputText };
