import styled from 'styled-components';

export const StyledComponent = styled.input`
  width: 100%;
  margin: 10px;
`;
