import styled from 'styled-components';
import { Row } from '../Row';

export const RowRight = styled(Row)`
  justify-content: flex-end;
`;
