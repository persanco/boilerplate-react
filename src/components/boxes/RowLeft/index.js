import styled from 'styled-components';
import { Row } from '../Row';

export const RowLeft = styled(Row)`
  justify-content: flex-start;
`;
