import styled from 'styled-components';

export const Button = styled.button`
  background: #010101;
  color: #fdfdfd;
  width: 100%;
  margin: 10px 10px;
  padding: 10px 10px;
  min-width: 100px;
`;
