import styled from 'styled-components';

export const ButtonText = styled.button`
  background: transparent;
  color: #fdfdfd;
  width: 100%;
  min-width: 100px;
`;
