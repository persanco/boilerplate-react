import React from 'react';
import PropTypes from 'prop-types';
import { NavBar } from '../NavBar';

const NavBarLanding = ({ onClick }) => <NavBar onClick={onClick} />;

NavBarLanding.propTypes = {
  onClick: PropTypes.func,
};

export { NavBarLanding };
