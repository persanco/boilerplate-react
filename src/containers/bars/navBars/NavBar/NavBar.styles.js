import styled from 'styled-components';
import { Text } from '../../../../components/typography/Text';
import { ButtonText as BtnText } from '../../../../components/buttons/ButtonText';

export const Container = styled.div`
  width: 100vw;
  background: #010101;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Menu = styled.div`
  width: 80%;
  max-width: 80vw;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  padding-right: 2%;
`;

export const TextSmall = styled(Text)`
  font-size: 10px;
`;

export const TextBig = styled(Text)`
  font-size: 16px;
  font-weight: 700;
`;

export const ButtonText = styled(BtnText)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0px 10px;
  width: 20%;
`;
