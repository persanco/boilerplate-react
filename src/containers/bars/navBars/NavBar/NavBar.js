import React from 'react';
import PropTypes from 'prop-types';
import { Logo } from '../../../../components/images/Logo';
import {
  Container, Menu, TextSmall, TextBig, ButtonText,
} from './NavBar.styles';

const NavBar = ({ options }) => (
  <Container>
    <Logo size="200" />
    <Menu>
      <ButtonText>
        <TextSmall>Eres un:</TextSmall>
        <TextBig>Cliente</TextBig>
      </ButtonText>
      <ButtonText>
        <TextSmall>Eres un:</TextSmall>
        <TextBig>Agente</TextBig>
      </ButtonText>
      <ButtonText>
        <TextSmall>Eres una:</TextSmall>
        <TextBig>Empresa</TextBig>
      </ButtonText>
    </Menu>
  </Container>
);

export { NavBar };
