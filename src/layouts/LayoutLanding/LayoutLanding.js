import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from '../Layout';
import { NavBarLanding } from '../../containers/bars/navBars/NavBarLanding';
import { Container } from './LayoutLanding.styles';

const LayoutLanding = ({ children, id = '', meta }) => (
  <Layout id={`LayoutLanding__${id}`} meta={meta}>
    <NavBarLanding onClick={() => console.log('Press NavBarLanding')} />
    <Container id={`LayoutLanding__${id}Container`}>{children}</Container>
  </Layout>
);

LayoutLanding.propTypes = {
  children: PropTypes.node.isRequired,
  id: PropTypes.string.isRequired,
  meta: PropTypes.object,
};

export { LayoutLanding };
