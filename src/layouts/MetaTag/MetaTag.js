import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { SEO } from '../../data/constants/SEO';

const MetaTag = ({ children, id, meta }) => {
  return (
    <Helmet>
      {meta?.title && <title>{`${SEO.PREFIX_TITLE} | ${meta.title}`}</title>}
      {meta?.description && <meta name="description" content={meta?.description ?? SEO.DESCRIPTION} />}
    </Helmet>
  );
};

MetaTag.propTypes = {
  meta: PropTypes.object,
};

export { MetaTag };
