import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { SEO } from '../../data/constants/SEO';
import { Container } from './Layout.styles';

const Layout = ({ children, id, meta }) => {
  return (
    <Container id={`Layout-${id}__`}>
      <Helmet>
        {meta?.title && <title>{`${SEO.PREFIX_TITLE} | ${meta.title}`}</title>}
        {meta?.description && <meta name="description" content={meta?.description ?? SEO.DESCRIPTION} />}
      </Helmet>
      {children}
    </Container>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  id: PropTypes.string.isRequired,
  meta: PropTypes.object,
};

export { Layout };
