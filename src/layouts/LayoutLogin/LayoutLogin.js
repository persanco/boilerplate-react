import React from 'react';
import PropTypes from 'prop-types';
import { MetaTag } from '../MetaTag';
import { Layout } from './LayoutLogin.styles';

const LayoutLogin = ({ children, id = '', meta }) => (
  <Layout id={`LayoutLogin__${id}`}>
    <MetaTag meta={meta} />
    {children}
  </Layout>
);

LayoutLogin.propTypes = {
  children: PropTypes.node.isRequired,
  id: PropTypes.string.isRequired,
  meta: PropTypes.object,
};

export { LayoutLogin };
