import styled from 'styled-components';
import { Box as BoxComponent } from '../../components/boxes/Box';

export const Layout = styled(BoxComponent)`
  height: 100vh;
  width: 100vw;
  /* justify-content: center; */
`;
