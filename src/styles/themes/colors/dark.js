export default {
  primary: '#FFFFFE',
  secondary: '#000001',
  tertiary: '#D0D0D0',
  quaternary: '#B0B0B0',
  background: '#000001',
  foreground: '#FFFFFE',
  dark: '#FFFFFE',
  light: '#000001',
  button: '#FFFFFE',
  buttonText: '#000001',
};
