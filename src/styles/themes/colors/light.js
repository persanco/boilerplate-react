export default {
  primary: '#000001',
  secondary: '#FFFFFE',
  tertiary: '#D0D0D0',
  quaternary: '#B0B0B0',
  background: '#FFFFFD',
  foreground: '#000001',
  dark: '#000001',
  light: '#FFFFFE',
  button: '#000001',
  buttonText: '#FFFFFE',
};
