const families = {
  sanserif: 'sans-serif',
  roboto: 'Roboto',
};

const sizes = {
  tiny: '0.5em',
  small: '0.75em',
  medium: '1em',
  large: '1.25em',
  big: '1.5em',

  h1: '32px',
  h2: '24px',
  h3: '18px',
  normal: '16px',
};

const styles = {
  title: `bold ${sizes.title}/${sizes.title} ${families.roboto}, serif`,
  subtitle: `bold ${sizes.title}/${sizes.title} ${families.roboto}, serif`,
  normal: `normal normal normal ${sizes.normal} ${families.roboto}, serif`,
  bold: `normal normal bold ${sizes.normal} ${families.roboto}, serif`,
  button: `bold ${sizes.title}/${sizes.title} ${families.roboto}, serif`,
};

export default {
  styles,
  families,
  sizes,
};
