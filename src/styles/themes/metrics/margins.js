const margin = {
  normal: '10px',
  small: '5px',
  large: '20px',
};

const marginHorizontal = {
  small: `0px ${margin.small}`,
  normal: `0px ${margin.normal}`,
  large: `0px ${margin.large}`,
};

const marginVertical = {
  small: `${margin.small} 0px`,
  normal: `${margin.normal} 0px`,
  large: `${margin.large} 0px`,
};

export default {
  margin,
  horizontal: marginHorizontal,
  vertical: marginVertical,
};
