import darkColors from './colors/dark';
import fonts from './fonts/fonts';
import margins from './metrics/margins';

export default {
  color: darkColors,
  fonts,
  margins,
};
